"""Test users"""
import pytest
from pydantic.error_wrappers import ValidationError
import dynamodb
from .conftest import delete_all_from_dynamo


def test_user_parsing():
    """Test that a user is parsed properly"""
    user_data = {
        # "username": "test user",
        # "email": "notanemail",
        "full_name": "this is a name",
        "disabled": False,
        "schedule_day": 0,
    }

    # When using an invalid email address, we should error
    with pytest.raises(ValidationError):
        broken_email_user = dynamodb.users.User(
            **user_data, username="valid_user", email="not_valid"
        )
        assert not broken_email_user

    # when using an invalid username, we should error
    with pytest.raises(ValidationError):
        broken_username_user = dynamodb.users.User(
            **user_data, username="not valid", email="valid@test.com"
        )
        assert not broken_username_user

    real_user = dynamodb.users.User(
        **user_data, username="valid_user", email="valid@test.com"
    )
    assert isinstance(real_user, dynamodb.users.User)


def test_create_user(create_users_handler: dynamodb.users.UserTable):
    """Test that a user is properly created"""
    delete_all_from_dynamo(create_users_handler)
    user = dynamodb.users.UserInDB(username="test", hashed_password="randomstring")
    created_user = create_users_handler.create_user(user)
    assert user.username == created_user.username
    assert isinstance(created_user, dynamodb.users.User)

    with pytest.raises(ValueError):
        create_users_handler.create_user(dynamodb.users.User(username="test"))


def test_get_user(create_users_handler: dynamodb.users.UserTable):
    """Test that a user is properly returned from the database"""
    delete_all_from_dynamo(create_users_handler)
    user = dynamodb.users.UserInDB(username="test", hashed_password="randomstring")
    create_users_handler.create_user(user)

    retrieved_user = create_users_handler.get_user("test")

    assert isinstance(retrieved_user, dynamodb.users.UserInDB)
    assert user.username == retrieved_user.username

    none_user = create_users_handler.get_user("notindatabase")
    assert none_user is None


def test_get_users_by_schedule(create_users_handler: dynamodb.users.UserTable):
    """Test that when queried by schedule day, only the correct users are returned."""
    delete_all_from_dynamo(create_users_handler)
    create_users_handler.create_user(
        dynamodb.users.UserInDB(
            username="1date", hashed_password="randomstring", schedule_day=1
        )
    )
    create_users_handler.create_user(
        dynamodb.users.UserInDB(
            username="2date", hashed_password="randomstring", schedule_day=2
        )
    )
    create_users_handler.create_user(
        dynamodb.users.UserInDB(
            username="3date", hashed_password="randomstring", schedule_day=2
        )
    )

    no_users = create_users_handler.get_users_by_schedule_day(0)
    one_users = create_users_handler.get_users_by_schedule_day(1)
    two_users = create_users_handler.get_users_by_schedule_day(2)

    assert no_users is None
    assert len(one_users) == 1
    assert len(two_users) == 2
