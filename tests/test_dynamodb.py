"""Test core dynamodb handler"""
import pytest
import dynamodb
from .conftest import TABLE_NAME


def test_no_table(create_dynamo_client):
    """Ensure that when a dynamo handler is created without a table or a table name, we error"""
    dynamo = create_dynamo_client
    with pytest.raises(ValueError):
        table_no_use = dynamodb.dynamodb.DynamoTable(dynamo)
        assert not table_no_use


def test_creating_when_table_exists(create_dynamo_client):
    """When a table already exists, we should reuse that table"""
    dynamo = create_dynamo_client
    initial_table = dynamodb.dynamodb.DynamoTable(dynamo, table_name=TABLE_NAME)
    secondary_table = dynamodb.dynamodb.DynamoTable(dynamo, table_name=TABLE_NAME)
    tertiary_table = dynamodb.dynamodb.DynamoTable(
        dynamo, ddb_table=initial_table.table
    )

    assert initial_table.table == secondary_table.table == tertiary_table.table


def test_table_created(create_dynamo_handler):
    """Test that we create a table with the correct schema when creating from scratch"""
    dynamo = create_dynamo_handler
    assert dynamo.table.table_name == TABLE_NAME
    assert dynamo.table.key_schema == [
        {"AttributeName": "pk", "KeyType": "HASH"},
        {"AttributeName": "sk", "KeyType": "RANGE"},
    ]
    assert dynamo.table.attribute_definitions == [
        {"AttributeName": "pk", "AttributeType": "S"},
        {"AttributeName": "sk", "AttributeType": "S"},
        {"AttributeName": "recipe_name", "AttributeType": "S"},
        {"AttributeName": "tags", "AttributeType": "S"},
        {"AttributeName": "username", "AttributeType": "S"},
        {"AttributeName": "schedule_day", "AttributeType": "N"},
    ]
    assert len(dynamo.table.global_secondary_indexes) == 3
    for index in dynamo.table.global_secondary_indexes:
        assert index.get("IndexName") in [
            "recipe_search",
            "user_search",
            "schedule_search",
        ]
