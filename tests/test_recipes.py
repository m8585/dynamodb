"""Test recipes"""
import pytest
from pydantic.error_wrappers import ValidationError
import dynamodb
from .conftest import delete_all_from_dynamo
from uuid import uuid4


BASIC_RECIPE = dynamodb.recipes.Recipe(
    recipe_name="test",
    estimated_duration=10,
    recipe_steps=[],
    ingredients=[],
    uuid="74d4a825-5f66-4195-abdb-343937b4c24f",
    serving_count=1,
)


def test_recipe_steps_handling():
    """Test that recipe steps are properly hashed for sets"""
    step_1 = dynamodb.recipes.RecipeStep(step_number=1, step_instructions="first step")
    step_2 = dynamodb.recipes.RecipeStep(step_number=2, step_instructions="second step")
    step_3 = dynamodb.recipes.RecipeStep(
        step_number=1, step_instructions="this will fail"
    )
    assert step_1 == step_3  # We only compare step numbers
    assert step_1 != step_2

    assert len(set([step_1, step_2, step_3])) == 2


def test_recipe_ingredients_handling():
    """Test that recipe ingredients are properly hashed for sets"""
    ingredient_1 = dynamodb.recipes.Ingredient(unit="unit", value=1, name="ingredient")
    ingredient_2 = dynamodb.recipes.Ingredient(unit="unit", value=1, name="ingredient2")
    ingredient_3 = dynamodb.recipes.Ingredient(unit="unit", value=1, name="ingredient")
    assert ingredient_1 == ingredient_3  # We only compare step numbers
    assert ingredient_1 != ingredient_2

    assert len(set([ingredient_1, ingredient_2, ingredient_3])) == 2


def test_recipe_alternatives_handling():
    """Test that recipe ingredients which have alternatives are properly hashed for sets"""
    ingredient_1 = dynamodb.recipes.BaseIngredient(
        unit="unit", value=1, name="ingredient"
    )
    ingredient_2 = dynamodb.recipes.BaseIngredient(
        unit="unit", value=1, name="ingredient2"
    )
    ingredient_3 = dynamodb.recipes.BaseIngredient(
        unit="unit", value=1, name="ingredient"
    )

    with pytest.raises(ValidationError):
        main_ingredient = dynamodb.recipes.Ingredient(
            unit="unit",
            value=1,
            name="main ingredient",
            alternatives=[ingredient_1, ingredient_2, ingredient_3],
        )
        assert not main_ingredient

    assert ingredient_1 == ingredient_3
    assert ingredient_1 != ingredient_2

    assert len(set([ingredient_1, ingredient_2, ingredient_3])) == 2


def test_recipes_parsed_properly():
    """Test that a recipe is properly parsed and modeled"""
    with pytest.raises(ValidationError):
        recipe = dynamodb.recipes.Recipe(
            recipe_name="testing recipe",
            estimated_duration="10",
            recipe_steps=[
                dynamodb.recipes.RecipeStep(
                    step_number=1, step_instructions="first step"
                ),
                dynamodb.recipes.RecipeStep(
                    step_number=2, step_instructions="second step"
                ),
                dynamodb.recipes.RecipeStep(
                    step_number=1, step_instructions="this will fail"
                ),
            ],
            ingredients=[
                dynamodb.recipes.Ingredient(
                    unit="units", value=1, name="test ingredient"
                ),
                dynamodb.recipes.Ingredient(
                    unit="units", value=1, name="other ingredient"
                ),
                dynamodb.recipes.Ingredient(
                    unit="units", value=1, name="test ingredient"
                ),
            ],
            uuid="f1042437-0a93-444a-966b-36a296894057",
        )
        assert not recipe

    with pytest.raises(ValidationError):
        # Ensure that a serving count != 1 raises
        recipe = dynamodb.recipes.Recipe(
            recipe_name="testing recipe",
            estimated_duration=10,
            uuid=uuid4(),
            recipe_steps=[dynamodb.recipes.RecipeStep(step_number=1, step_instructions="first step")],
            ingredients=[dynamodb.recipes.Ingredient(unit="unites", value=1, name="test ingredient")],
            serving_count=2
        )

        assert not recipe

    recipe = dynamodb.recipes.Recipe(
        recipe_name="test",
        estimated_duration=10,
        recipe_steps=[],
        ingredients=[],
        uuid="74d4a825-5f66-4195-abdb-343937b4c24f",
        serving_count=1,
    )
    assert recipe


def test_get_no_recipes(create_recipes_handler: dynamodb.recipes.RecipeTable):
    """Test that when there are no recipes, we simply return None"""
    absent_random_recipe = create_recipes_handler.get_random_recipe()
    assert absent_random_recipe is None
    absent_recipe = create_recipes_handler.get_recipe_by_id("test")
    assert absent_recipe is None


def test_add_recipe(create_recipes_handler: dynamodb.recipes.RecipeTable):
    """Test that we can add a recipe and when queried, its data is as expected"""
    create_response = create_recipes_handler.create_recipe(BASIC_RECIPE)
    assert create_response == BASIC_RECIPE
    recipe_from_uuid = create_recipes_handler.get_recipe_by_id(
        recipe_id=BASIC_RECIPE.uuid
    )
    assert recipe_from_uuid == BASIC_RECIPE
    delete_all_from_dynamo(create_recipes_handler)


def test_recipe_search(create_recipes_handler: dynamodb.recipes.RecipeTable):
    """Test that when searching the following happen:
    * When there are no recipes, an empty list should be returned
    * When there are reciped but no matches, an empty list should be returned
    * When there are recipes and at least one match, all matches should be returned
    """
    _search_string = "searching"
    _matching_recipe1 = dynamodb.recipes.Recipe(
        recipe_name="searching recipe",
        estimated_duration=10,
        recipe_steps=[],
        ingredients=[],
        uuid="9e7d03e2-bacd-4771-8cb4-0ec1b27390c8",
        serving_count=1,
    )
    _matching_recipe2 = dynamodb.recipes.Recipe(
        recipe_name="test",
        estimated_duration=10,
        recipe_steps=[],
        ingredients=[],
        uuid="17013312-fc24-49b5-b0aa-a4fd2e810893",
        tags=["searching", "is", "cool"],
        serving_count=1,
    )

    # When there are no recipes, return None
    no_recipes_search = create_recipes_handler.search_recipes(_search_string)
    assert no_recipes_search == []

    # When there are no matching recipes, return None
    create_recipes_handler.create_recipe(BASIC_RECIPE)
    nonmatching_search = create_recipes_handler.search_recipes(_search_string)
    assert nonmatching_search == []

    create_recipes_handler.create_recipe(_matching_recipe1)
    create_recipes_handler.create_recipe(_matching_recipe2)
    matching_search = create_recipes_handler.search_recipes(_search_string)
    assert len(matching_search) == 2
    assert all(
        [
            a == b
            for a, b in zip([_matching_recipe1, _matching_recipe2], matching_search)
        ]
    )
    delete_all_from_dynamo(create_recipes_handler)


def test_updating_recipe(create_recipes_handler: dynamodb.recipes.RecipeTable):
    """Test that updating a recipe works as expected"""
    created_recipe = create_recipes_handler.create_recipe(BASIC_RECIPE)
    assert created_recipe == BASIC_RECIPE

    updated_recipe = BASIC_RECIPE.copy()
    updated_recipe.tags = ["these", "are", "some", "tags"]
    updated_recipe = create_recipes_handler.create_recipe(updated_recipe)
    assert updated_recipe != created_recipe
    assert updated_recipe.tags == ["these", "are", "some", "tags"]
    delete_all_from_dynamo(create_recipes_handler)


def test_random_recipes(create_recipes_handler: dynamodb.recipes.RecipeTable):
    """Test that random recipes are pulled properly"""
    delete_all_from_dynamo(create_recipes_handler)

    random_no_recipes = create_recipes_handler.get_random_recipe()
    assert random_no_recipes is None

    create_recipes_handler.create_recipe(BASIC_RECIPE)
    second_recipe = BASIC_RECIPE
    second_recipe.recipe_name = "second recipe"
    create_recipes_handler.create_recipe(second_recipe)

    random_recipe = create_recipes_handler.get_random_recipe()
    assert isinstance(random_recipe, dynamodb.recipes.Recipe)
