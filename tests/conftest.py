"""Fixtures for pytest"""
import os
from moto import mock_dynamodb2
import boto3
import pytest
import dynamodb

TABLE_NAME = "Test_table"


@pytest.fixture(name="aws_credentials", scope="module")
def fixture_aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"
    os.environ["AWS_DEFAULT_REGION"] = "eu-west-2"


@pytest.fixture(name="create_dynamo_client", scope="module")
def fixture_create_dynamo_client() -> boto3.resource:
    """Create a dynamodb client"""
    with mock_dynamodb2():
        yield boto3.resource("dynamodb", region_name="eu-west-2")


@pytest.fixture(name="create_dynamo_handler", scope="module")
def fixture_create_dynamo_handler(
    create_dynamo_client,
) -> dynamodb.dynamodb.DynamoTable:
    """Create an instance of the base dynamodb class"""
    return dynamodb.dynamodb.DynamoTable(
        connection=create_dynamo_client, table_name=TABLE_NAME
    )


@pytest.fixture(name="create_users_handler", scope="module")
def fixture_create_users_handler(create_dynamo_handler) -> dynamodb.users.UserTable:
    """Create an instance of the UserTable class"""
    return dynamodb.users.UserTable(create_dynamo_handler)


@pytest.fixture(name="create_recipes_handler", scope="module")
def fixture_create_recipes_handler(
    create_dynamo_handler,
) -> dynamodb.recipes.RecipeTable:
    """Create an instance of the RecipeTable class"""
    return dynamodb.recipes.RecipeTable(create_dynamo_handler)


def delete_all_from_dynamo(dynamo_handler: dynamodb.dynamodb.DynamoTable):
    """Delete all entries from a dynamodb table"""
    table = dynamo_handler.table
    table_scan = table.scan()

    for item in table_scan["Items"]:
        table.delete_item(Key={"pk": item["pk"], "sk": item["sk"]})
