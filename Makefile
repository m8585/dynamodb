cov:
	poetry run pytest --cov=dynamodb/ tests/ --cov-report html --cov-report term -s

test:
	poetry run pytest -s