"""Models and dynamodb handler for user queries"""
from typing import List, Optional
from uuid import uuid4
from pydantic import BaseModel, EmailStr, validator
from .dynamodb import DynamoTable


class User(BaseModel):
    """A user"""

    username: str
    email: Optional[EmailStr] = None
    full_name: Optional[str] = None
    disabled: Optional[bool] = None
    schedule_day: Optional[int] = None

    @validator("username")
    @classmethod
    def username_contains_no_spaces(cls, username_to_validate):
        """Ensure that a username does not contain spaces"""
        no_spaces = username_to_validate.replace(" ", "")
        assert username_to_validate == no_spaces
        return username_to_validate


class RegisteringUser(User):
    """A user registering"""

    password: str


class UserInDB(User):
    """A user in the database"""

    hashed_password: str


class Token(BaseModel):
    """A JWT Token"""

    access_token: str
    token_type: str


class TokenData(BaseModel):
    """The data in a JWT token"""

    username: Optional[str] = None


class UserTable:
    """Handle the users

    Parameters:
    ddb_table (DynamoTable): An instance of a DynamoTable
    """

    def __init__(self, ddb_table: DynamoTable):
        self.table = ddb_table.table

    def create_user(self, user: UserInDB) -> User:
        """Create a new user in dynamodb

        Parameters:
        user (UserInDB): A user

        Returns:
        User: A user
        """

        if not isinstance(user, UserInDB):
            raise ValueError("User must be a UserInDB object", user)

        response = self.table.update_item(
            Key={"pk": "USERS", "sk": str(uuid4())},
            UpdateExpression=f"SET {', '.join([f'#{key} = :{key}' for key in user.dict().keys()])}",
            ExpressionAttributeNames={f"#{key}": key for key in user.dict().keys()},
            ExpressionAttributeValues={
                f":{key}": item for key, item in user.dict().items()
            },
        )

        if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
            raise ValueError(  # pragma: no cover # This can't be properly handled until we resolve the create/update split
                "Something went wrong and I cba to figure it out right now", response
            )

        return User(**user.dict())

    def get_user(self, username: str) -> UserInDB:
        """Get a user from the database

        Parameters:
        username (str): The username of a user

        Returns:
        UserInDB: A user in the database
        """

        response = self.table.query(
            IndexName="user_search",
            KeyConditionExpression="#pk = :pk And #username = :username",
            ExpressionAttributeNames={
                "#pk": "pk",
                "#username": "username",
            },
            ExpressionAttributeValues={
                ":pk": "USERS",
                ":username": username,
            },
        )

        if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
            raise ValueError(  # pragma: no cover # I'm not sure how to test this yet
                "Something went wrong and I cba to figure it out right now", response
            )

        if not response["Items"]:
            return None

        return UserInDB(**response["Items"][0])

    def get_users_by_schedule_day(
        self, schedule_day: int
    ) -> List[User]:  # TODO make schedule_day an enum
        """Get a list of users based on which day of the week
         they're scheduled to receive recipes

        Parameters:
        schedule_day (int): Which day of the week, per cron numbers

        Returns:
        List[User]: A list of users who are scheduled to receive recipes
        """

        response = self.table.query(
            IndexName="schedule_search",
            KeyConditionExpression="#scheduleday = :scheduleday",
            ExpressionAttributeNames={
                "#scheduleday": "schedule_day",
            },
            ExpressionAttributeValues={
                ":scheduleday": schedule_day,
            },
        )

        if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
            raise ValueError(  # pragma: no cover # I'm not sure how to test this yet
                "Something went wrong and I cba to figure it out right now", response
            )

        if not response["Items"]:
            return None

        return [User(**user) for user in response["Items"]]
