"""A base Dynamodb class"""
import boto3
from boto3.dynamodb import table
from botocore.exceptions import ClientError


class DynamoTable:
    """Handle a dynamodb table

    Parameters:
    connection (boto3.resource): A boto3 dynamodb resource
    ddb_table (boto3.dynamodb.table): A dynamodb table
    """

    def __init__(
        self,
        connection: boto3.resource = None,
        ddb_table: table = None,
        table_name: str = None,
    ):
        self.dynamodb = connection

        if not ddb_table:
            if not table_name:
                raise ValueError("either ddb_table or table_name must be provided")
            current_table = self.dynamodb.Table(table_name)

            # This is the only viable method to check if a dynamodb table exists
            try:
                if current_table.table_status:
                    self.table = current_table
            except ClientError:
                self.table = self.__create_table(table_name)
        else:
            self.table = ddb_table

    def __create_table(self, table_name) -> table:
        """Create a new empty dynamodb table.

        Returns:
        boto3.dynamodb.table: A dynamodb table
        """

        ddb_table = self.dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {"AttributeName": "pk", "KeyType": "HASH"},
                {"AttributeName": "sk", "KeyType": "RANGE"},
            ],
            AttributeDefinitions=[
                {"AttributeName": "pk", "AttributeType": "S"},
                {"AttributeName": "sk", "AttributeType": "S"},
                {"AttributeName": "recipe_name", "AttributeType": "S"},
                {"AttributeName": "tags", "AttributeType": "S"},
                {"AttributeName": "username", "AttributeType": "S"},
                {"AttributeName": "schedule_day", "AttributeType": "N"},
            ],
            BillingMode="PAY_PER_REQUEST",
            GlobalSecondaryIndexes=[
                {
                    "IndexName": "recipe_search",
                    "KeySchema": [
                        {
                            "AttributeName": "recipe_name",
                            "KeyType": "HASH",
                        },
                        {"AttributeName": "tags", "KeyType": "RANGE"},
                    ],
                    "Projection": {
                        "ProjectionType": "ALL",
                    },
                },
                {
                    "IndexName": "user_search",
                    "KeySchema": [
                        {"AttributeName": "pk", "KeyType": "HASH"},
                        {"AttributeName": "username", "KeyType": "RANGE"},
                    ],
                    "Projection": {
                        "ProjectionType": "ALL",
                    },
                },
                {
                    "IndexName": "schedule_search",
                    "KeySchema": [
                        {"AttributeName": "schedule_day", "KeyType": "HASH"},
                        {
                            "AttributeName": "username",
                            "KeyType": "RANGE",
                        },
                    ],
                    "Projection": {
                        "ProjectionType": "ALL",
                    },
                },
            ],
        )

        return ddb_table
