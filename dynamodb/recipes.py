"""Handle dynamodb"""
from typing import Optional, List
from uuid import uuid4
from pydantic import BaseModel, validator
from pydantic.types import UUID4, conlist
from .dynamodb import DynamoTable

attribute_map = {
    list: "L",
    str: "S",
    int: "N",
}


class RecipeStep(BaseModel):
    """A step for a recipe"""

    step_number: int
    step_instructions: str

    def __hash__(self):
        return hash(self.step_number)

    def __eq__(self, other):
        return self.step_number == other.step_number


class BaseIngredient(BaseModel):
    """An ingredient for a recipe"""

    unit: str
    value: int
    name: str
    required: bool = True

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name


class Ingredient(BaseIngredient):
    """An ingredient for a recipe"""

    alternatives: Optional[conlist(BaseIngredient, unique_items=True)] = []

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name


class Ingredient(BaseIngredient):
    """An ingredient for a recipe"""

    alternatives: Optional[conlist(BaseIngredient, unique_items=True)] = []

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name


class Recipe(BaseModel):
    """A Recipe"""

    recipe_name: str
    tags: Optional[conlist(str, unique_items=True)] = []
    # Must be converted to str like#this#per#tag
    uuid: UUID4 # PK
    recipe_steps: conlist(RecipeStep, unique_items=True)
    ingredients: conlist(Ingredient, unique_items=True)
    estimated_duration: int
    serving_count: int

    @validator("serving_count")
    @classmethod
    def ensure_single_serving_count(cls, serving_count_to_validate) -> int:
        """Ensure that serving count is equal to one"""
        assert serving_count_to_validate == 1
        return serving_count_to_validate


class RecipeTable:
    """Handle the recipes

    Parameters:
    ddb_table (DynamoTable): An instance of a DynamoTable
    """

    def __init__(self, ddb_table: DynamoTable):
        self.table = ddb_table.table

    def parse_recipe(self, recipe_dict: dict) -> Recipe:  # TODO remove from class?
        """
        Parse a dynamodb object into a recipe

        Parameters:
        recipe_dict (dict): A dynamodb dict object of a recipe

        Returns:
        Recipe: A recipe
        """
        recipe_dict["tags"] = [tag for tag in recipe_dict["tags"].split("#") if tag]
        if not recipe_dict["tags"]:
            del recipe_dict["tags"]
        recipe_dict["uuid"] = recipe_dict["sk"]

        return Recipe(**recipe_dict)

    def get_random_recipe(self) -> Recipe:
        """
        Get a random recipe from dynamodb

        Returns:
        Recipe: A recipe.
        """
        # TODO Is this valid??
        if self.table.item_count == 0:
            return None

        response = self.table.scan(
            Limit=1,
            ExclusiveStartKey={"pk": "RECIPE", "sk": str(uuid4())},
        )

        if not len(response["Items"]) == 1 and self.table.item_count > 0:
            return (
                self.get_random_recipe()
            )  # pragma: no cover # this accounts for an edge case in dynamo we can't consistently test

        return self.parse_recipe(response["Items"][0])

    def search_recipes(self, search_string: str) -> List[Recipe]:
        """
        Get a specific recipe

        Parameters:
        search_string (str): The string to search for

        Returns:
        List[Recipe]: A list of
        """
        search_list = search_string.split(" ")

        name_search = " OR ".join(
            f"contains(#recipename, :search{index})"
            for index in range(0, len(search_list))
        )
        tag_search = " OR ".join(
            f"contains(#tagsstring, :search{index})"
            for index in range(0, len(search_list))
        )

        response = self.table.scan(
            IndexName="recipe_search",
            FilterExpression=f"{name_search} OR {tag_search}",
            ExpressionAttributeNames={
                "#recipename": "recipe_name",
                "#tagsstring": "tags",
            },
            ExpressionAttributeValues={
                f":search{index}": search_list[index]
                for index in range(0, len(search_list))
            },
        )
        return [self.parse_recipe(recipe) for recipe in response["Items"]]

    def get_recipe_by_id(self, recipe_id: str) -> Recipe:
        """
        Get a specific recipe

        Parameters:
        recipe_id (str): The uuid of a recipe.

        Returns:
        Recipe: A recipe.
        """

        response = self.table.get_item(Key={"pk": "RECIPE", "sk": str(recipe_id)})
        if recipe := response.get("Item"):
            return self.parse_recipe(recipe)
        else:
            return None

    def create_recipe(self, recipe: Recipe) -> Recipe:
        """
        Create a recipe in dynamodb.

        Parameters:
        recipe (Recipe): A recipe

        Returns:
        Recipe: A recipe.
        """

        recipe_dict = recipe.dict()
        if recipe_dict["tags"]:
            recipe_dict["tags"] = "#".join(recipe_dict.get("tags", []))
        else:
            recipe_dict["tags"] = "#"
        del recipe_dict["uuid"]

        response = self.table.update_item(
            Key={"pk": "RECIPE", "sk": str(recipe.uuid)},
            UpdateExpression=f"SET {', '.join([f'#{key} = :{key}' for key in recipe_dict.keys()])}",
            ExpressionAttributeNames={f"#{key}": key for key in recipe_dict.keys()},
            ExpressionAttributeValues={
                f":{key}": item for key, item in recipe_dict.items()
            },
        )

        if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
            raise ValueError(  # pragma: no cover # This can't be properly handled until we resolve the create/update split
                "Something went wrong and I cba to figure it out right now", response
            )

        return recipe
